package builder;

public class BassBuilder extends StringedBuilder{
    @Override
    public void addNeck() {
        stringed.setNeck("Gto", "HTG-M");
    }

    @Override
    public void addBridge() {
        stringed.setBridge("MFeel", "Medium Bridge");
    }

    @Override
    public void addDeck() {
        stringed.setDeck("WakeUp", "Jorgia");
    }

}
