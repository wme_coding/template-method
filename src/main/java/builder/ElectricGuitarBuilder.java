package builder;

public class ElectricGuitarBuilder extends StringedBuilder {

    @Override
    public void addNeck() {
        stringed.setNeck("Gear4Music", "RW");
    }

    @Override
    public void addBridge() {
        stringed.setBridge("Gotoh", "GE103B-N");
    }

    @Override
    public void addDeck() {
        stringed.setDeck("Rosewood", "M3");
    }
}
