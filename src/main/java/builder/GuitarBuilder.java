package builder;

public class GuitarBuilder extends StringedBuilder{
    @Override
    public void addNeck() {
        stringed.setNeck("Yamaha", "Oak");
    }

    @Override
    public void addBridge() {
        stringed.setBridge("Like", "R99");
    }

    @Override
    public void addDeck() {
        stringed.setDeck("HOLD", "I6");
    }
}
