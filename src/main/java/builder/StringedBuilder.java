package builder;

import stringed.Stringed;

public abstract class StringedBuilder {
    protected Stringed stringed;

    public StringedBuilder() {
        stringed = new Stringed();
    }

    public final Stringed makeStringed(){
        addNeck();
        addBridge();
        addDeck();
        return stringed;
    }

    public abstract void addNeck();
    public abstract void addBridge();
    public abstract void addDeck();
}
