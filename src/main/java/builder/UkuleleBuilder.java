package builder;

public class UkuleleBuilder extends StringedBuilder{
    @Override
    public void addNeck() {
        stringed.setNeck("Mint", "KekW");
    }

    @Override
    public void addBridge() {
        stringed.setBridge("Jk", "4Head");
    }

    @Override
    public void addDeck() {
        stringed.setDeck("Ola", "GachiGasm");
    }
}
