package stringed;

import components.Bridge;
import components.Deck;
import components.Neck;

public class Stringed {
    private StringedType type;
    private Bridge bridge;
    private Neck neck;
    private Deck deck;

    public Stringed() {
        type = null;
        bridge = null;
        neck = null;
        deck = null;
    }

    public Bridge getBridge() {
        return bridge;
    }

    public void setBridge(String brand, String model) {
        bridge.setBrand(brand);
        bridge.setModel(model);
    }

    public Neck getNeck() {
        return neck;
    }

    public void setNeck(String brand, String model) {
        neck.setBrand(brand);
        neck.setModel(model);
    }

    public Deck getDeck() {
        return deck;
    }

    public void setDeck(String brand, String model) {
        deck.setBrand(brand);
        deck.setModel(model);
    }
}
