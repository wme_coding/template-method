package stringed;

public enum  StringedType {
    UKULELE, GUITAR, BASS, ELECTRIC_GUITAR
}
